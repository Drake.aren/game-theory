(ns games-theory.map)

(def place-state
  {:champaign {:difficult 0
               :fertility 10
               :passability 10}
   :hill      {:difficult 0
               :fertility 8
               :passability 8}
   :mountain  {:difficult 1
               :fertility 7
               :passability 6}
   :desert
   :semidesert
   :prairie
   :swamp
   :cold-swamp
   :tundra})

(defn generate
  "Define who will be map generated"
  [x-limit y-limit map])

  ; ((fn [x-limit map]
  ;   ((fn [y-limit map]
  ;     (println x-limit y-limit))
  ;    y-limit map))
  ;  x-limit map))

  ; (let [map [[]]]
  ;   (for [x (take x-limit (iterate inc 1))]
  ;     (for [y (take y-limit (iterate inc 1))]
  ;       (assoc-in map [x y]
  ;         {:type :champaign
  ;          :palced nil})))))

  ; (if (and (zero? x-limit)
  ;          (zero? y-limit))
  ;   map
  ;   (recur (dec x-limit)
  ;          (dec y-limit)
  ;          (assoc-in map [x-limit y-limit]
  ;            {:type :champaign
  ;             :palced nil}))))
