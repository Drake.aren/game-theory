(ns games-theory.action)

(def describe
  {:food {:time-per-item 1
          :requre-per-item {:wood 1
                            :worker 1}}
   :wood {:time-per-item 1
          :requre-per-item {:worker 1}}})

(def init
  "Define initialize of action"
  [keyword value]
  {:item keyword
   :count value})

(defn add
  "Define added doing into do-list"
  [town action]
  (assoc town
    :do-list (concat (:do-list town)
                     (cons action '()))))

(defn discharge
  "Define how doing will do"
  [town]
  (let [total-time (* (:population town)
                      (:work-time town))]
    (->> (:do-list town)
         (map (fn [e] (println e))))))
