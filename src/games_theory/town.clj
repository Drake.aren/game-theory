(ns games-theory.town)

(defn init-town
  "Define initialize of town"
  [map]
  {:x (rand-int (:x-limit map))
   :y (rand-int (:y-limit map))
   :population 10
   :wood 50
   :food 25
   :work-time 8
   :do-list (list)})

(defn starve
  "Function define how people starve by 1 day"
  [town]
  (assoc town
    :food (- (:food town)
             (* (:population town) 0.1))))
