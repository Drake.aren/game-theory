(ns games-theory.weather)

(def time-in-day 24)

(def seasons
  [{:title :autem
    :days 96}
   {:title :summer
    :days 97}
   {:title :spring
    :days 85}
   {:title :winter
    :days 91}])

(defn cur-season
  "Canculate current season"
  [calendar seasons]
  (let [cur-day (:day calendar)]
    (cond
      (> cur-day (reduce + (map #(:days %1) (nthnext seasons 1)))
        (:title (get seasons 3)))
      (> cur-day (reduce + (map #(:days %1) (nthnext seasons 2)))
        (:title (get seasons 2)))
      (> cur-day (reduce + (map #(:days %1) (nthnext seasons 3)))
        (:title (get seasons 1)))
      :else (:title (nth seasons 0)))))
