(ns games-theory.game)

(def state
  (atom {:calendar {:year 0 :day 0}
         :max-players 6
         :difficult 0 ;; default, low difficult
         :players []}))

(defn set-difficult
  "Setup difficult of game"
  [level]
  (swap! state assoc
    :difficult level))

(defn init-player
  "Added player in to game-state"
  [name race]
  (if (< (.length (:players @state))
         (:max-players @state))
    (swap! state assoc
        :players (conj (:players @state)
                       {:name name
                        :race race}))
    {:text "maximal player limit"
     :code 1}))
